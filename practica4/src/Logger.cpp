#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(){
	mkfifo ("/tmp/TuberiaLogger",0777);
	int pipe=open("/tmp/TuberiaLogger",O_RDONLY);
	int dato_leido;
	while(1) {

		char cad[200];
		dato_leido=read (pipe,cad, sizeof(cad));
		if (dato_leido)
		printf("%s",cad);
	}
	close (pipe);
	unlink("/tmp/TuberiaLogger");
	return 0;

}
